package mandocapps.travelexperts_app.dataObj;

/**
 * Created by Hernan Ayarza on 10/4/2017.
 */

public class User {

    //properties
    private String userName;
    private String userPass;


    //Constructors

    public User() {
    }

    public User(String u, String p) {
        super();
        userName = u;
        userPass = p;
    }

    //methods

    //Setters and Getters

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }
}