package mandocapps.travelexperts_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//Made by Hernan Ayarza on 05/10/17
//This was supposed to be the Customer side of the app

public class Menu_Cust extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu__cust);
        Toast toast = Toast.makeText(getApplicationContext(), "Logged in as Customer", Toast.LENGTH_SHORT);
        toast.show();

        //Instantiate buttons

        ImageView logout = findViewById(R.id.logoutBtn);




        //Create events


        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent main = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(main);
            }
        });



    }
}
