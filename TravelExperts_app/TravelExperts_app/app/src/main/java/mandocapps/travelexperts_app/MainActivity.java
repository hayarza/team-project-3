package mandocapps.travelexperts_app;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import mandocapps.travelexperts_app.dataObj.User;

//Made by Hernan Ayarza on 05/10/17

public class MainActivity extends Activity {

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().getDecorView().setBackgroundColor(Color.parseColor("#2C2F33"));

        //Buttons and EditTexts
        final Button loginBtn = (Button) findViewById(R.id.loginBtn);
        final EditText username = (EditText) findViewById(R.id.txtUsername);
        final EditText password = (EditText) findViewById(R.id.txtPassword);
        final TextView responseTXT = (TextView) findViewById(R.id.responseTXT);




        //Connection Status
        // check if you are connected or not
        if(isConnected()){
            Toast.makeText(MainActivity.this, "You are connected", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(MainActivity.this, "No Connection", Toast.LENGTH_SHORT).show();
        }


        //Create onClick Event for Login Button
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final User user = new User();

                user.setUserName(username.getText().toString());
                user.setUserPass(password.getText().toString());

                Gson test = new Gson();
                JSONObject jo = new JSONObject();
                try {
                    jo.put("userName", user.getUserName());
                    jo.put("userPass", user.getUserPass());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                String jsonobject = test.toJson(user);



                // Instantiate the RequestQueue.
                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                String url = "http://10.163.112.11:8080/travelexapi/webapi/author";



                JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, url, jo, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d("Response", response.toString());
                                try {
                                    Integer agentID = Integer.parseInt(response.get("agentId").toString());
                                    Integer custID = Integer.parseInt(response.get("customerId").toString());
                                    if (agentID > 0) {
                                        Intent agentMenu = new Intent(getApplicationContext(), Menu_Agent.class);
                                        startActivity(agentMenu);

                                    } else if (custID > 0) {
                                        Intent custMenu = new Intent(getApplicationContext(), Menu_Cust.class);
                                        startActivity(custMenu);
                                    } else  {
                                        Toast toast = Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT);
                                        toast.show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //   Handle Error
                                Log.d("Error.Response", error.toString());
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        //headers.put("Content-Type", "application/json");
                        //headers.put("User-agent", System.getProperty("http.agent"));
                        return headers;
                    }
                };
                queue.add(postRequest);
            }
        });

    }




    //Checking for Connection for testing purposes
    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
}
