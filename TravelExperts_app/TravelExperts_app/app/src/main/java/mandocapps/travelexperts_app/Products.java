package mandocapps.travelexperts_app;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mandocapps.travelexperts_app.dataObj.Product;


//Made by Hernan Ayarza on 05/10/17

public class Products extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        //instantiate list

        final ListView products = findViewById(R.id.lvProducts);

        RequestQueue queue = Volley.newRequestQueue(this);

        //Volley GET request
        final String url = "http://10.163.112.11:8080/travelexapi/webapi/products";

        // prepare the Request
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        Log.d("Response", response.toString());
                        ArrayList list = new ArrayList();

                        //StringBuffer sb = new StringBuffer();
                        final ArrayAdapter<Product> adapter = new ArrayAdapter<Product>(getApplicationContext(),
                                android.R.layout.simple_list_item_1, list);

                        try{
                            //JSONArray jsonArray = (JSONArray) response;

                            for(int i=0; i < response.length(); i++){
                                JSONObject a = response.getJSONObject(i);
                                Product prods = new Product(
                                        a.getInt("productId"),
                                        a.getString("prodName")
                                );
                                list.add(prods);
                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                        products.setAdapter(adapter);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                    }
                }
        );

        // add it to the RequestQueue
        queue.add(getRequest);


    }
}
