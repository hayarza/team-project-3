package mandocapps.travelexperts_app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.icu.text.NumberFormat;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import mandocapps.travelexperts_app.dataObj.Package;

import static android.R.id.list;


//Made by Hernan Ayarza on 05/10/17


public class Package_Edit extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package__edit);

        //instantiate views
        final TextView pkgID = findViewById(R.id.pkgID_txt);
        final EditText pkgName = findViewById(R.id.pkgNameTXT);
        final EditText pkgStartDate = findViewById(R.id.pkgStartDate_txt);
        final EditText pkgEndDate = findViewById(R.id.pkgEndDate_txt);
        final EditText pkgDesc = findViewById(R.id.pkgDesc_txt);
        final EditText pkgBasePrice = findViewById(R.id.pkgBasePrice_txt);
        final EditText pkgAgCom = findViewById(R.id.pkgAgencyCom_txt);
        final Button editBtn = findViewById(R.id.btnEdit);
        final Button deleteBtn = findViewById(R.id.btnDelete);


        //get package ID from intent
        Intent pkgEdit = getIntent();
        pkgID.setText(pkgEdit.getStringExtra("position"));
        final String position = pkgEdit.getStringExtra("position");


        //Get Package

        RequestQueue queue = Volley.newRequestQueue(this);

        //Volley GET request
        final String url = "http://10.163.112.11:8080/travelexapi/webapi/packages/" + position;

        // prepare the Request
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        // display response
                        Log.d("Response", response.toString());


                        try{

                            JSONObject a = response;
                            Package packs = new Package(
                                    a.getInt("packageId"),
                                    a.getString("pkgName"),
                                    a.getString("pkgStartDate"),
                                    a.getString("pkgEndDate"),
                                    a.getString("pkgDesc"),
                                    a.getDouble("pkgBasePrice"),
                                    a.getDouble("pkgAgencyCommission")
                            );

                            //display data
                            DecimalFormat df1 = new DecimalFormat("####.00");

                            pkgName.setText(packs.getPkgName());
                            pkgStartDate.setText(packs.getPkgStartDate());
                            pkgEndDate.setText(packs.getPkgEndDate());
                            pkgDesc.setText(packs.getPkgDesc());
                            pkgBasePrice.setText(df1.format(packs.getPkgBasePrice()));
                            pkgAgCom.setText(df1.format(packs.getPkgAgencyCommission()));



                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                       // packages.setAdapter(adapter);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                    }
                }
        );

        // add it to the RequestQueue
        queue.add(getRequest);


        //UPDATE BUTTOn

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Create Package out of textfields
                final Package pkg = new Package();
                pkg.setPackageId(Integer.parseInt(pkgID.getText().toString()));
                pkg.setPkgName(pkgName.getText().toString());
                pkg.setPkgStartDate(pkgStartDate.getText().toString());
                pkg.setPkgDesc(pkgDesc.getText().toString());
                pkg.setPkgEndDate(pkgEndDate.getText().toString());
                pkg.setPkgBasePrice(Double.parseDouble(pkgBasePrice.getText().toString()));
                pkg.setPkgAgencyCommission(Double.parseDouble(pkgAgCom.getText().toString()));

                //convert package Object to JSON
                JSONObject jo = new JSONObject();
                try {

                    jo.put("packageId", pkg.getPackageId());
                    jo.put("pkgName", pkg.getPkgName());
                    jo.put("pkgStartDate", pkg.getPkgStartDate());
                    jo.put("pkgEndDate", pkg.getPkgEndDate());
                    jo.put("pkgDesc", pkg.getPkgDesc());
                    jo.put("pkgBasePrice", pkg.getPkgBasePrice());
                    jo.put("pkgAgencyCommission", pkg.getPkgAgencyCommission());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //Do the little dance

                // Instantiate the RequestQueue.
                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                String url = "http://10.163.112.11:8080/travelexapi/webapi/packages/" + position;



                JsonObjectRequest putRequest = new JsonObjectRequest(Request.Method.PUT, url, jo, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Response", response.toString());
                        Toast.makeText(Package_Edit.this, "Congratulations! You updated your LIFE!!", Toast.LENGTH_LONG).show();
                        Intent packagesAgent = new Intent(getApplicationContext(), Packages_Agent.class);
                        startActivity(packagesAgent);
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                //   Handle Error
                                Log.d("Error.Response", error.toString());
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<String, String>();
                        //headers.put("Content-Type", "application/json");
                        //headers.put("User-agent", System.getProperty("http.agent"));
                        return headers;
                    }
                };

                queue.add(putRequest);


            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());

                builder
                        .setMessage("Ruin your life?")
                        .setPositiveButton("Yes",  new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                // Yes-code
                                    //DELETE
                                //Volley DELETE request
                                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
                                final String url = "http://10.163.112.11:8080/travelexapi/webapi/packages/" + position;
                                StringRequest dr = new StringRequest(Request.Method.DELETE, url,
                                        new Response.Listener<String>()
                                        {
                                            @Override
                                            public void onResponse(String response) {
                                                // response
                                                Toast.makeText(Package_Edit.this, "Congratulations! You just ruined your life!", Toast.LENGTH_LONG).show();
                                                Intent packagesAgent = new Intent(getApplicationContext(), Packages_Agent.class);
                                                startActivity(packagesAgent);
                                            }
                                        },
                                        new Response.ErrorListener()
                                        {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                // error.

                                            }
                                        }
                                );
                                queue.add(dr);

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        })
                        .show();
            }
        });

    }


}
