package mandocapps.travelexperts_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

//Made by Hernan Ayarza on 05/10/17

public class Menu_Agent extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_agent);
        Toast toast = Toast.makeText(getApplicationContext(), "Logged in as Agent", Toast.LENGTH_SHORT);
        toast.show();

        //Instantiate buttons
        ImageView products = findViewById(R.id.productsBtn);
        ImageView packages = findViewById(R.id.packagesBtn);
        ImageView logout = findViewById(R.id.logoutBtn);
        TextView logoutLbl = findViewById(R.id.logoutTXT);
        TextView productsLbl = findViewById(R.id.productsTXT);
        TextView packagesLbl = findViewById(R.id.packagesTXT);


        //Create events

        products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent productsMenu = new Intent(getApplicationContext(), Products.class);
                startActivity(productsMenu);
            }
        });
        packages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent packagesAgent = new Intent(getApplicationContext(), Packages_Agent.class);
                startActivity(packagesAgent);
            }
        });

        productsLbl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent productsMenu = new Intent(getApplicationContext(), Products.class);
                startActivity(productsMenu);
            }
        });

        packagesLbl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent packagesAgent = new Intent(getApplicationContext(), Packages_Agent.class);
                startActivity(packagesAgent);
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent main = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(main);
            }
        });

        logoutLbl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent main = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(main);
            }
        });


    }
}
