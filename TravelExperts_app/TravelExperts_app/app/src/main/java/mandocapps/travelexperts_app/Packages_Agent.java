package mandocapps.travelexperts_app;

import android.app.Activity;
import android.content.Intent;
import android.icu.text.DateFormat;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;

import mandocapps.travelexperts_app.dataObj.Package;
import mandocapps.travelexperts_app.dataObj.Product;

import static android.provider.AlarmClock.EXTRA_MESSAGE;


//Made by Hernan Ayarza on 05/10/17

public class Packages_Agent extends Activity {

    ArrayList<Package> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_packages__agent);

        //instantiate list

        final ListView packages = findViewById(R.id.lvPackages);

        RequestQueue queue = Volley.newRequestQueue(this);

        //Volley GET request
        final String url = "http://10.163.112.11:8080/travelexapi/webapi/packages";

        // prepare the Request
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response) {
                        // display response
                        Log.d("Response", response.toString());


                        //StringBuffer sb = new StringBuffer();
                        final ArrayAdapter<Package> adapter = new ArrayAdapter<Package>(getApplicationContext(),
                                android.R.layout.simple_list_item_1, list);

                        try{

                            for(int i=0; i < response.length(); i++){
                                JSONObject a = response.getJSONObject(i);
                                Package packs = new Package(
                                        a.getInt("packageId"),
                                        a.getString("pkgName"),
                                        a.getString("pkgStartDate"),
                                        a.getString("pkgEndDate"),
                                        a.getString("pkgDesc"),
                                        a.getDouble("pkgBasePrice"),
                                        a.getDouble("pkgAgencyCommission")
                                );
                                list.add(packs);

                            }
                        } catch (JSONException e){
                            e.printStackTrace();
                        }
                        packages.setAdapter(adapter);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error.Response", error.toString());
                    }
                }
        );

        // add it to the RequestQueue
        queue.add(getRequest);

        packages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent pkgEdit = new Intent(getApplicationContext(), Package_Edit.class);
                //Integer position = i + 1;
                int pkgId = list.get(i).getPackageId();
                pkgEdit.putExtra("position", pkgId+"");
                startActivity(pkgEdit);
            }
        });

    }


}
