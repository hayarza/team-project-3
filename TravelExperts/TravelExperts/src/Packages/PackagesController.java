/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 *
 * @author Reza 
 * 
 */

package Packages;

import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import services.TravelExService;
import dataObj.Package;
import dataObj.PackageObs;
import dataObj.ProductObs;
import travelexperts.Validator;


/**
*
* @author Reza
* @ Edited by Derek
* @ Trubleshoot by Hernan and Derek
*/



public class PackagesController implements Initializable {

	@FXML
    private ResourceBundle resources;

    @FXML
    private URL location;
    @FXML
    private TableView<PackageObs> tvPackage;

    @FXML
    private TableColumn<PackageObs, Number> PackageId;

    @FXML
    private TableColumn<PackageObs, String> PackageName;
    @FXML
    private Button btnConfirmAdd;

    @FXML
    private Button btnConfirmupdate;

    @FXML
    private Button btnConfirmDelete;

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnAdd;

    @FXML
    private Button btnUpdate;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnMain1;

    @FXML
    private Button btnProduct;

    @FXML
    private Button btnExit;

    @FXML
    private TextField tfPackageId;

    @FXML
    private Button btnSearch;

    @FXML
    private TextField tfPackageName;

    @FXML
    private DatePicker dpStart;

    @FXML
    private DatePicker dpEnd;

    @FXML
    private TextField tfBasePrice;

    @FXML
    private TextField tfCommission;

    @FXML
    private TextField tfDescreption;
	
    
    private NumberFormat formatter = NumberFormat.getCurrencyInstance();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    	updateTable();
    	
      //List<Package> pkgs = new ArrayList<Package>();

/*        try {
			pkgs = TravelExService.getPackages();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
        for (Package p:pkgs) {
        	System.out.println(p.getPkgName());
        }*/

        
    }    

    // Get data from service and show in the table
    
    @FXML
    private void searchProductId(ActionEvent event) {
    	
    	//NumberFormat formatter = NumberFormat.getCurrencyInstance();
    	
    	if (!dataIsValid2 ())
    	{
    		return;
    	}
    	else
    	{
    		
    	
    	
        btnAdd.setVisible(true);
        btnDelete.setVisible(true);
        btnUpdate.setVisible(true);
        btnCancel.setVisible(false);
       
        
        int packageId = Integer.parseInt(tfPackageId.getText());
    	
    	Package pkg = TravelExService.getPackage(packageId);
    	
    	tfPackageId.setText(pkg.getPackageId()+"");
    	tfPackageName.setText(pkg.getPkgName());
        tfDescreption.setText(pkg.getPkgDesc());
        dpStart.setValue(pkg.getPkgStartDate().toLocalDate());
        dpEnd.setValue(pkg.getPkgEndDate().toLocalDate());
        
        tfBasePrice.setText(formatter.format(pkg.getPkgBasePrice()));
        tfCommission.setText(formatter.format(pkg.getPkgAgencyCommission()));
    	}
    }
        
    
    
    @FXML
    private void btnaddProSupp(ActionEvent event) {
        btnAdd.setVisible(false);
        btnDelete.setVisible(false);
        btnUpdate.setVisible(false);
        btnCancel.setVisible(true);
      

        
    }
    
    @FXML
    private void btndelProSupp(ActionEvent event) {
        btnAdd.setVisible(false);
        btnDelete.setVisible(false);
        btnUpdate.setVisible(false);
        btnConfirmDelete.setVisible(false);
        btnCancel.setVisible(true);
      
    }

    // for show message
    private void msgbox(String s){
 	   JOptionPane.showMessageDialog(null, s);
 	}
    
    // confirm to add a package
    @FXML
    private void doAdd(ActionEvent event) {
    	
    	if (dataIsValid1 ())
    	{
    	ConfirmDoAdd();	
    	btnAdd.setVisible(true);
        btnDelete.setVisible(true);
        btnUpdate.setVisible(true);
        btnConfirmAdd.setVisible(false);
        btnConfirmupdate.setVisible(false);
        btnConfirmDelete.setVisible(false);
        btnCancel.setVisible(false);
       
        tfBasePrice.setEditable(false);
        tfCommission.setEditable(false);
        tfDescreption.setEditable(false);
        tfPackageName.setEditable(false);
        dpStart.setEditable(false);
        dpEnd.setEditable(false);
        tfPackageId.setEditable(true);
        dpEnd.setDisable(true);
        dpStart.setDisable(true);
        tfPackageId.setDisable(false);
        btnSearch.setDisable(true);
        
        tfPackageId.setText("");
        tfBasePrice.setText("");
        tfCommission.setText("");
        tfDescreption.setText("");
        tfPackageName.setText("");

    	}
    }
    
        @FXML
        private void ConfirmDoAdd() {
        
        Package pkg = new Package(
        tfPackageName.getText(),
    	Date.valueOf(dpStart.getValue()),
    	Date.valueOf(dpEnd.getValue()),
    	tfDescreption.getText(),
    	Double.parseDouble(tfBasePrice.getText()),
    	Double.parseDouble(tfCommission.getText()));
        
      
        
        Package check = TravelExService.insertPackage(pkg);
        
        
        if (check.equals(pkg)) {
        	msgbox("There was a problem with adding the new package.");
        }
        else 
        {
        	msgbox("The record has been added");

        }
// To reload table
        updateTable();
        tfPackageId.setVisible(true);
    }

        // to update a package
    @FXML
    private void doUpdate(ActionEvent event) {
    	
    	if (!dataIsValid1 ())
    	{
    		return;
    	}
    	else
    	{
    	
        btnAdd.setVisible(true);
        btnDelete.setVisible(true);
        btnUpdate.setVisible(true);
        btnConfirmAdd.setVisible(false);
        btnConfirmupdate.setVisible(false);
        btnConfirmDelete.setVisible(false);
        btnCancel.setVisible(false);
        
        tfBasePrice.setEditable(false);
        tfCommission.setEditable(false);
        tfDescreption.setEditable(false);
        tfPackageName.setEditable(false);
        dpStart.setEditable(false);
        dpEnd.setEditable(false);
        tfPackageId.setEditable(true);
        dpEnd.setDisable(true);
        dpStart.setDisable(true);
        

        
   

        Package pkg = new Package();
        pkg.setPackageId(Integer.parseInt(tfPackageId.getText()));
        pkg.setPkgName(tfPackageName.getText());
    	pkg.setPkgDesc(tfDescreption.getText());
    	pkg.setPkgStartDate(Date.valueOf(dpStart.getValue()));
    	pkg.setPkgEndDate( Date.valueOf(dpEnd.getValue()));
    	try {
			pkg.setPkgBasePrice((formatter.parse(tfBasePrice.getText())).doubleValue());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	try {
			pkg.setPkgAgencyCommission((formatter.parse(tfCommission.getText())).doubleValue());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	

        
    	Package chk= TravelExService.updatePackage(pkg);
    	
    	 if (chk.equals(pkg)) {
         	msgbox("There was a problem with Edit the package.");
         }
         else 
         {
         	msgbox("The record has been updated");
         	
         }
    	}
        tfPackageId.setText("");
        tfBasePrice.setText("");
        tfCommission.setText("");
        tfDescreption.setText("");
        tfPackageName.setText("");
    	updateTable();

    }
    
    // to delete a package

    @FXML
    private void doDelete(ActionEvent event) {
        btnAdd.setVisible(true);
        btnDelete.setVisible(true);
        btnUpdate.setVisible(true);
        btnConfirmAdd.setVisible(false);
        btnConfirmupdate.setVisible(false);
        btnConfirmDelete.setVisible(false);
        btnCancel.setVisible(false);
        

        tfPackageId.setEditable(true);
        
        int packageId = Integer.parseInt(tfPackageId.getText());

    	boolean pkg = TravelExService.deletePackage(packageId);
    	
    	 if (!pkg) {
          	msgbox("There was a problem with Delete the package.");
          }
          else 
          {
          	msgbox("The record has been deleted");
          	
          }
         tfPackageId.setText("");
         tfBasePrice.setText("");
         tfCommission.setText("");
         tfDescreption.setText("");
         tfPackageName.setText("");
    	 updateTable();
    }

    // to cancel add, edit, ot delete
    @FXML
    private void doCancel(ActionEvent event) {
        btnAdd.setVisible(true);
        btnDelete.setVisible(true);
        btnUpdate.setVisible(true);
        btnConfirmAdd.setVisible(false);
        btnConfirmupdate.setVisible(false);
        btnConfirmDelete.setVisible(false);
        btnCancel.setVisible(false);
      
        tfBasePrice.setEditable(false);
        tfCommission.setEditable(false);
        tfDescreption.setEditable(false);
        tfPackageName.setEditable(false);
        dpStart.setEditable(false);
        dpEnd.setEditable(false);
        tfPackageId.setEditable(true);
        dpEnd.setDisable(true);
        dpStart.setDisable(true);
        tfPackageId.setDisable(false);


    }

    
    // add button
    @FXML
    private void btnadd(ActionEvent event) {
        btnAdd.setVisible(false);
        btnDelete.setVisible(false);
        btnUpdate.setVisible(false);
        btnConfirmAdd.setVisible(true);
        btnConfirmupdate.setVisible(false);
        btnConfirmDelete.setVisible(false);
        btnCancel.setVisible(true);


        tfPackageId.setEditable(false);
        tfPackageId.setText("");
        tfPackageId.setDisable(true);
        tfBasePrice.setEditable(true);
        tfBasePrice.setText("");
        tfCommission.setEditable(true);
        tfCommission.setText("");
        tfDescreption.setEditable(true);
        tfDescreption.setText("");
        tfPackageName.setEditable(true);
        tfPackageName.setText("");
        dpStart.setEditable(true);
        dpStart.setValue(null);
        dpEnd.setEditable(true);
        dpEnd.setValue(null);
        dpEnd.setDisable(false);
        dpStart.setDisable(false);
    }
// update button
    @FXML
    private void btnUpdate(ActionEvent event) {
    	
    	if (!dataIsValid2 ())
    	{
    		return;
    	}
    	else
    	{
        btnAdd.setVisible(false);
        btnDelete.setVisible(false);
        btnUpdate.setVisible(false);
        btnConfirmAdd.setVisible(false);
        btnConfirmupdate.setVisible(true);
        btnConfirmDelete.setVisible(false);
        btnCancel.setVisible(true);
       
        tfPackageId.setEditable(false);
        tfBasePrice.setEditable(true);
        tfCommission.setEditable(true);
        tfDescreption.setEditable(true);
        tfPackageName.setEditable(true);
        dpStart.setEditable(true);
        dpEnd.setEditable(true);
        dpEnd.setDisable(false);
        dpStart.setDisable(false);
        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        
        int packageId = Integer.parseInt(tfPackageId.getText());
    	
    	Package pkg = TravelExService.getPackage(packageId);
    	
    	tfPackageId.setText(pkg.getPackageId()+"");
    	tfPackageName.setText(pkg.getPkgName());
        tfDescreption.setText(pkg.getPkgDesc());
        dpStart.setValue(pkg.getPkgStartDate().toLocalDate());
        dpEnd.setValue(pkg.getPkgEndDate().toLocalDate());

        tfBasePrice.setText(formatter.format(pkg.getPkgBasePrice()));
        tfCommission.setText(formatter.format(pkg.getPkgAgencyCommission()));
        
    	}

	}
    
// delete button
    @FXML
    private void btnDelete(ActionEvent event) {
    	if (!dataIsValid2 ())
    	{
    		return;
    	}
    	else
    	{
        btnAdd.setVisible(false);
        btnDelete.setVisible(false);
        btnUpdate.setVisible(false);
        btnConfirmAdd.setVisible(false);
        btnConfirmupdate.setVisible(false);
        btnConfirmDelete.setVisible(true);
        btnCancel.setVisible(true);
       
        tfPackageId.setEditable(false);
        
        NumberFormat formatter = NumberFormat.getCurrencyInstance();

 int packageId = Integer.parseInt(tfPackageId.getText());
    	
	Package pkg = TravelExService.getPackage(packageId);
	
	tfPackageId.setText(pkg.getPackageId()+"");
	tfPackageName.setText(pkg.getPkgName());
 tfDescreption.setText(pkg.getPkgDesc());
 dpStart.setValue(pkg.getPkgStartDate().toLocalDate());
 dpEnd.setValue(pkg.getPkgEndDate().toLocalDate());

 tfBasePrice.setText(formatter.format(pkg.getPkgBasePrice()));
 tfCommission.setText(formatter.format(pkg.getPkgAgencyCommission()));
    	}
    }
// go back to main page
    @FXML
    private void goBack(ActionEvent event) {
        try {
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/travelexperts/mainMenu.fxml"));
                  Parent root = (Parent) fxmlLoader.load();
                  Stage stage = new Stage();
                 stage.setScene(new Scene(root));  
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
            } catch(Exception e) {
               e.printStackTrace();
              } 
             Stage stage = (Stage) btnMain1.getScene().getWindow();
             stage.close();
    }
    
// shift to product page
    @FXML
    private void goProduct(ActionEvent event) {
        {
        try {
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Products/products.fxml"));
                  Parent root = (Parent) fxmlLoader.load();
                  Stage stage = new Stage();
                 stage.setScene(new Scene(root));  
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
            } catch(Exception e) {
               e.printStackTrace();
              } 
             Stage stage = (Stage) btnProduct.getScene().getWindow();
             stage.close();
    }
    } 

    // to exit application
    @FXML
    private void SceneExit(ActionEvent event) {
        System.exit(0);
    }
    
    

    // validate fields
    public boolean dataIsValid1 () {
  	
    LocalDate startDate = dpStart.getValue();  
    LocalDate EndDate = dpEnd.getValue();
    String Baseprice = tfBasePrice.getText().toString();
    String Commission = tfCommission.getText().toString();

    	
        return (Validator.isPresent(tfPackageName, "Package Name ") &&
        		(Validator.isPresentDate(dpStart, "Start Date") &&
				(Validator.isPresentDate(dpEnd, "End Date") &&
				(Validator.isValidDateRange(startDate, EndDate, "") &&
                (Validator.isPresent(tfBasePrice, "Base price") &&
                (Validator.isNumeric(Baseprice, tfBasePrice, "Base price") && 
                (Validator.isPresent(tfCommission, "Agency commission") &&
                (Validator.isNumeric(Commission, tfCommission, "Commission") &&
		        (Validator.isPresent(tfDescreption, "Description")

                )))))))));          
    } 
    // validate package id
    public boolean dataIsValid2 ()
    {
    	 return (Validator.isPresent(tfPackageId, "Package Id ") &&
                 (Validator.isNumber(tfPackageId, "Package Id ")));
    }

    // method to reload 
    public void updateTable() {
    	tvPackage.setItems(null);
    	List<Package> pkgs = TravelExService.getPackages();

    	ObservableList<PackageObs> obsPackages = FXCollections.observableArrayList();
    	
    	for (Package p:pkgs) {
    		obsPackages.add(new PackageObs(p.getPackageId(),p.getPkgName()));
    	}
        
        PackageId.setCellValueFactory(cellData -> cellData.getValue().packageIdProperty());
        PackageName.setCellValueFactory(cellData -> cellData.getValue().packageNameProperty());
    	
    	tvPackage.setItems(obsPackages);
    }
    


}
    


