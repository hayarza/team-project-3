/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.SyncInvoker;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dataObj.Package;
import dataObj.Product;
/**
 *
 * @author Derek
 * @Edited By Reza
 */
public class TravelExService {
	
    // web service template url = http://10.163.101.152:8080/travelexapi/webapi/packages
	// setup the various targes for the web uri calls
    static Client client = ClientBuilder.newClient();
    static WebTarget baseTarget = client.target("http://10.187.244.81:8080/travelexapi/webapi/");

    static WebTarget packagesTarget = baseTarget.path("packages");
    static WebTarget singlePackageTarget = packagesTarget.path("{packageId}");
   
    static WebTarget productsTarget = baseTarget.path("products");
    static WebTarget singleProductTarget = productsTarget.path("{productId}");
   
    //Method to get single package with id
    public static Package getPackage (int packageId) {
        Package pkg = singlePackageTarget
        		.resolveTemplate("packageId", packageId)
        		.request(MediaType.APPLICATION_JSON)
        		.get(Package.class);
        
        return pkg;
    }
    
    //Method to get all packages
    public static List<Package> getPackages () {
        List<Package> pkgs = packagesTarget
        		.request(MediaType.APPLICATION_JSON)
        		.get(new GenericType<List<Package>>() {});
        
        
        
        
        return pkgs;
    }
    
    //Method to insert a new package record with package object
    public static Package insertPackage (Package p) {
     	Response response = packagesTarget
    			.request()
    			.post(Entity.json(p));
				
		Package pkg = response.readEntity(Package.class);
    	
    	return pkg;
    }
    
    //method to update package with id and package object
    public static Package updatePackage (Package p) {
    	Response response = singlePackageTarget
    			.resolveTemplate("packageId", p.getPackageId())
    			.request()
    			.put(Entity.json(p));
    	
    	Package pkg = response.readEntity(Package.class);
    	
    	return pkg;
    }
    
    //method to delete package with id
    public static boolean deletePackage (int packageId) {
    	Response response = singlePackageTarget
    			.resolveTemplate("packageId", packageId)
    			.request()
    			.delete();
    	
    	if (response.readEntity(int.class) ==1) {
    		return true;
    	}
    	
    	return false;
    }
    
    //method to get all products
    public static List<Product> getProducts () {
    	List<Product> products= productsTarget
    			.request(MediaType.APPLICATION_JSON)
    			.get(new GenericType<List<Product>>() {});
    	
    	return products;
    }
    
    //method to get single product by product ID
    public static Product getProduct (int productId) {
    	Product product = singleProductTarget
        		.resolveTemplate("productId", productId)
        		.request(MediaType.APPLICATION_JSON)
        		.get(Product.class);
        
        return product;
    }
}
    
  