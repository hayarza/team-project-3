/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Products;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import dataObj.Product;
import dataObj.ProductObs;
import javafx.beans.Observable;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import services.TravelExService;
import travelexperts.Validator;

/**
 * 
 *
 * @author Reza 
 * @ Edited by Fabian
 * 
 */
public class ProductsController implements Initializable {

    @FXML
    private TextField tfProdId;
    @FXML
    private Button btnSearch;
    @FXML
    private TextField tfProdName;
    @FXML
    private TableView<ProductObs> tableviewProd;
    @FXML
    private TableColumn<ProductObs, Number> ProductId;

    @FXML
    private TableColumn<ProductObs, String> productName;
    @FXML
    private Button btnConfirmAdd;
    @FXML
    private Button btnConfirmupdate;
    @FXML
    private Button btnConfirmDelete;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnAdd;
    @FXML
    private Button btnUpdate;
    @FXML
    private Button btnDelete;
    @FXML
    private Button btnMain1;
    @FXML
    private Button btnPackages1;
    @FXML
    private Button btnExit;
    @FXML
    private ComboBox<?> cbsupplier;
    @FXML
    private TableView<?> tableviewSupplier;
    @FXML
    private VBox frameSupplier;
    @FXML
    private Label lblsup1;
    @FXML
    private Label lblsup2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    	
    	List<Product> products = TravelExService.getProducts();
    	
    	
    	ObservableList<ProductObs> obsProducts = FXCollections.observableArrayList();
    	
    	for (Product p:products) {
    		obsProducts.add(new ProductObs(p.getProductId(),p.getProdName()));
    	}
        
    	ProductId.setCellValueFactory(cellData -> cellData.getValue().productIdProperty());
    	productName.setCellValueFactory(cellData -> cellData.getValue().prodNameProperty());
    	
    	tableviewProd.setItems(obsProducts);
    	
        
        

    }    
    // for making message box
    
    private void msgbox(String s){
  	   JOptionPane.showMessageDialog(null, s);
  	}

    // search product
    @FXML
    private void searchProductId(ActionEvent event) {
    	
    	if (!dataIsValid2 ())
    	{
    		msgbox("Please fill the package Id text box");
    		return;
    	}
    	else
    	{
    		
    	int productId = Integer.parseInt(tfProdId.getText());
    	
    	Product prod = TravelExService.getProduct(productId);
    	
    	tfProdId.setText(prod.getProductId()+"");
    	tfProdName.setText(prod.getProdName());
    	}
    }

    @FXML
    private void doAdd(ActionEvent event) {

        btnAdd.setVisible(true);
        btnDelete.setVisible(true);
        btnUpdate.setVisible(true);
        btnConfirmAdd.setVisible(false);
        btnConfirmupdate.setVisible(false);
        btnConfirmDelete.setVisible(false);
        btnCancel.setVisible(false);
        cbsupplier.setDisable(true);
        tableviewSupplier.setDisable(true);
        lblsup1.setDisable(true);
        lblsup2.setDisable(true);
        
                
        
    }

    @FXML
    private void doUpdate(ActionEvent event) {

        btnAdd.setVisible(true);
        btnDelete.setVisible(true);
        btnUpdate.setVisible(true);
        btnConfirmAdd.setVisible(false);
        btnConfirmupdate.setVisible(false);
        btnConfirmDelete.setVisible(false);
        btnCancel.setVisible(false);
        cbsupplier.setDisable(true);
        tableviewSupplier.setDisable(true);
        lblsup1.setDisable(true);
        lblsup2.setDisable(true);
    }

    @FXML
    private void doDelete(ActionEvent event) {
        btnAdd.setVisible(true);
        btnDelete.setVisible(true);
        btnUpdate.setVisible(true);
        btnConfirmAdd.setVisible(false);
        btnConfirmupdate.setVisible(false);
        btnConfirmDelete.setVisible(false);
        btnCancel.setVisible(false);
        cbsupplier.setDisable(true);
        tableviewSupplier.setDisable(true);
        lblsup1.setDisable(true);
        lblsup2.setDisable(true);
        
    }

    @FXML
    private void doCancel(ActionEvent event) {
        btnAdd.setVisible(true);
        btnDelete.setVisible(true);
        btnUpdate.setVisible(true);
        btnConfirmAdd.setVisible(false);
        btnConfirmupdate.setVisible(false);
        btnConfirmDelete.setVisible(false);
        btnCancel.setVisible(false);
        cbsupplier.setDisable(true);
        tableviewSupplier.setDisable(true);
        lblsup1.setDisable(true);
        lblsup2.setDisable(true);
    }

    @FXML
    private void btnadd(ActionEvent event) {
        btnAdd.setVisible(false);
        btnDelete.setVisible(false);
        btnUpdate.setVisible(false);
        btnConfirmAdd.setVisible(true);
        btnConfirmupdate.setVisible(false);
        btnConfirmDelete.setVisible(false);
        btnCancel.setVisible(true);
        cbsupplier.setDisable(false);
        tableviewSupplier.setDisable(false);
        lblsup1.setDisable(false);
        lblsup2.setDisable(false);        
        
    }

    @FXML
    private void btnUpdate(ActionEvent event) {
        btnAdd.setVisible(false);
        btnDelete.setVisible(false);
        btnUpdate.setVisible(false);
        btnConfirmAdd.setVisible(false);
        btnConfirmupdate.setVisible(true);
        btnConfirmDelete.setVisible(false);
        btnCancel.setVisible(true);
        cbsupplier.setDisable(false);
        tableviewSupplier.setDisable(false);
        lblsup1.setDisable(false);
        lblsup2.setDisable(false); 
    }

    @FXML
    private void btnDelete(ActionEvent event) {
        btnAdd.setVisible(false);
        btnDelete.setVisible(false);
        btnUpdate.setVisible(false);
        btnConfirmAdd.setVisible(false);
        btnConfirmupdate.setVisible(false);
        btnConfirmDelete.setVisible(true);
        btnCancel.setVisible(true);
        cbsupplier.setDisable(false);
        tableviewSupplier.setDisable(false);
        lblsup1.setDisable(false);
        lblsup2.setDisable(false); 
    }

    // go back to main menu
    @FXML
    private void goBack(ActionEvent event) {
                         try {
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/travelexperts/mainMenu.fxml"));
                  Parent root = (Parent) fxmlLoader.load();
                  Stage stage = new Stage();
                 stage.setScene(new Scene(root));  
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
            } catch(Exception e) {
               e.printStackTrace();
              } 
             Stage stage = (Stage) btnMain1.getScene().getWindow();
             stage.close();
    }

    // shift to packages
    @FXML
    private void goPackages(ActionEvent event) {
        {
        try {
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Packages/packages.fxml"));
                  Parent root = (Parent) fxmlLoader.load();
                  Stage stage = new Stage();
                 stage.setScene(new Scene(root));  
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
            } catch(Exception e) {
               e.printStackTrace();
              } 
             Stage stage = (Stage) btnPackages1.getScene().getWindow();
             stage.close();
    }
        
    }

    // exit application
    @FXML
    private void SceneExit(ActionEvent event) {
        System.exit(0);
        
    }
    
    // validation for product ID
    public boolean dataIsValid2 ()
    {
    	 return (Validator.isPresent(tfProdId, "Product Id ") &&
                 (Validator.isNumber(tfProdId, "Product Id ")));
    }
    
}
