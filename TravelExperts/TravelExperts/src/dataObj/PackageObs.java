package dataObj;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class PackageObs {

	private final SimpleIntegerProperty packageId;
	private final SimpleStringProperty packageName;
	
	public PackageObs (int pId, String pName) {
		this.packageId = new SimpleIntegerProperty(pId);
		this.packageName = new SimpleStringProperty(pName);
		
	}


	public int packageId() {
		return packageId.get();
	}

	public void setpackageId(int packageId) {
		this.packageId.set(packageId); 
	}
	
	public IntegerProperty packageIdProperty() {
		return packageId;
	}

	public String getpackageName() {
		return packageName.get();
	}

	public void setpackageName(String packageName) {
		this.packageName.set(packageName);
	}
	
	public StringProperty packageNameProperty() {
		return packageName;
	}
	
	
}
