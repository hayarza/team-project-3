package dataObj;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProductObs {

	private final SimpleIntegerProperty productId;
	private final SimpleStringProperty prodName;
	
	public ProductObs (int pId, String pName) {
		this.productId = new SimpleIntegerProperty(pId);
		this.prodName = new SimpleStringProperty(pName);
		
	}


	public int getProductId() {
		return productId.get();
	}

	public void setProductId(int productId) {
		this.productId.set(productId); 
	}
	
	public IntegerProperty productIdProperty() {
		return productId;
	}

	public String getProdName() {
		return prodName.get();
	}

	public void setProdName(String prodName) {
		this.prodName.set(prodName);
	}
	
	public StringProperty prodNameProperty() {
		return prodName;
	}
	
	
}
