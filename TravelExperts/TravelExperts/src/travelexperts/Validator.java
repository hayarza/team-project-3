

/**
 * 
 *
 * @author Reza 
 * 
 */

package travelexperts;

import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import static javax.swing.JOptionPane.showMessageDialog;

import java.time.LocalDate;

// this is the validator class for the Packages and products fields

public  class Validator {
    
    // data is mandatory
	
    public static boolean isPresent(TextField tf, String tfname)
    {
        if (tf.getText() == null) 
        {
           showMessageDialog(null, tfname + " must be filled");
           tf.requestFocus();
            return false;
        } else
            {
            if (tf.getText().trim().isEmpty()) 
            {
                showMessageDialog(null, tfname + " must be filled");
                tf.requestFocus();
                return false;
            }  else
                return true;
            
        }
    }
    //Validator for date pickers
    
    public static boolean isPresentDate(DatePicker dp, String dpname)
    {
        if (dp.getValue() == null) 
        {
           showMessageDialog(null, dpname + " must be chosen");
           dp.requestFocus();

            return false;
      
        }  else
                return true;
        
 }
    
    //Validator for Start and end date
    /*    
    public static boolean isValidDateRange(Date startDate, Date endDate, String Msg) {
        // false if either value is null
        if (startDate == null || endDate == null) { return false; }
        
        // true if they are equal
        if (startDate.equals(endDate)) { return true; }
        
        
        // true if endDate after startDate
        if (endDate.after(startDate)) { return true; }
        
        return false;
    }
    */
    
    // field must be numeric
    public static boolean isNumber(TextField tf, String tfname)
    {
        try {
            double n = Integer.parseInt(tf.getText());
            return true;
            
            } catch (NumberFormatException  e) {
                showMessageDialog(null, tfname + " must be a number.");
                tf.requestFocus();

                return false;
        }
        
    }

	public static boolean isValidDateRange(LocalDate startDate, LocalDate endDate, String msg) {
		// TODO Auto-generated method stub
    if (startDate == null || endDate == null) { return false; }
        
        // true if they are equal
        if (startDate.equals(endDate)) { return true; }
        
        
        // true if endDate after startDate
        if (endDate.isAfter(startDate)) { return true; }
        
        else 
        	showMessageDialog(null, " The End date must be greater than Start date");

        	return false;
		
	}
	
	public static boolean isNumeric(String str, TextField tf, String tfname)
	{
		if (str.matches("\\$?\\-?\\d{0,3},?\\d{0,3}\\.?\\d{2}?"))  { return true; }
	 
		else
			 showMessageDialog(null, tfname + " must be a number");
        tf.requestFocus();

			 return false;  //match a number with optional '-' and decimal.
	}
}
