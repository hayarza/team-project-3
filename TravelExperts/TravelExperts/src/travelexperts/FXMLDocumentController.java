/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package travelexperts;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Reza
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Button btnExit;
    @FXML
    private Button btnPackages;
    @FXML
    private Button btnProducts;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    


    @FXML
    private void StartProducts(ActionEvent event) {
                 try {
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Products/products.fxml"));
                  Parent root = (Parent) fxmlLoader.load();
                  Stage stage = new Stage();
                 stage.setScene(new Scene(root));  
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
            } catch(Exception e) {
               e.printStackTrace();
              }  
             Stage stage = (Stage) btnProducts.getScene().getWindow();
             stage.close();
    
    }

    @FXML
    private void StartPackages(ActionEvent event) {
                
        try {
         FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/Packages/packages.fxml"));
                  Parent root = (Parent) fxmlLoader.load();
                  Stage stage = new Stage();
                 stage.setScene(new Scene(root));  
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.show();
            } catch(Exception e) {
               e.printStackTrace();
              }  
             Stage stage = (Stage) btnPackages.getScene().getWindow();
             stage.close();
    }

    @FXML
    private void btnExit(ActionEvent event) {
        System.exit(0);
    }


    
}
