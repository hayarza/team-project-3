package dataBaseConn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dataObj.Customer;

public class CustomerDB {
	//TODO: create local variables
	
	//TODO: Create getCustomer method
	
	public static List<Customer> getAllCustomers () throws ClassNotFoundException, SQLException {
		
		Connection conn = TravelExpertsDB.getConnection();
		
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM customers");
		List<Customer> custs = new ArrayList<Customer>();
		Customer cust;
		
		while (rs.next()) {
			cust = new dataObj.Customer();
			
			cust.setCustomerId(rs.getInt(1));
			cust.setCustFirstName(rs.getString(2));
			cust.setCustLastName(rs.getString(3));
			cust.setCustAddress(rs.getString(4));
			cust.setCustCity(rs.getString(5));
			cust.setCustProv(rs.getString(6));
			cust.setCustPostal(rs.getString(7));
			cust.setCustCountry(rs.getString(8));
			cust.setCustHomePhone(rs.getString(9));
			cust.setCustBusPhone(rs.getString(10));
			cust.setCustEmail(rs.getString(11));
			cust.setAgentId(rs.getInt(12));
			
			custs.add(cust);
		}
		
		TravelExpertsDB.closeConnection(conn);
		return custs;
		
	}
	
	public static Customer getCustomer(int CustomerId) throws ClassNotFoundException, SQLException {
		
		Customer cust = new Customer();
		
		Connection conn = TravelExpertsDB.getConnection();
		PreparedStatement stmt = conn.prepareStatement("SELECT * FROM customers WHERE CustomerId=?");
		stmt.setInt(1, CustomerId);
		ResultSet rs = stmt.executeQuery();
		
		if (rs.next()) {
			cust.setCustomerId(rs.getInt(1));
			cust.setCustFirstName(rs.getString(2));
			cust.setCustLastName(rs.getString(3));
			cust.setCustAddress(rs.getString(4));
			cust.setCustCity(rs.getString(5));
			cust.setCustProv(rs.getString(6));
			cust.setCustPostal(rs.getString(7));
			cust.setCustCountry(rs.getString(8));
			cust.setCustHomePhone(rs.getString(9));
			cust.setCustBusPhone(rs.getString(10));
			cust.setCustEmail(rs.getString(11));
			cust.setAgentId(rs.getInt(12));
		}
		
		TravelExpertsDB.closeConnection(conn);
		
		return cust;
	}

	
	/*public static Customer insertCustomer(Customer cust) throws ClassNotFoundException, SQLException {

		Connection conn = TravelExpertsDB.getConnection();
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO Customers"
				+ " (custName, custStartDate, custEndDate, custDesc, custBasePrice, custAgencyCommission)" 
				+ " VALUES (?,?,?,?,?,?)");
		stmt.setString(1, cust.getcustName());
		stmt.setDate(2, cust.getcustStartDate());
		stmt.setDate(3, cust.getcustEndDate());
		stmt.setString(4, cust.getcustDesc());
		stmt.setDouble(5, cust.getcustBasePrice());
		stmt.setDouble(6, cust.getcustAgencyCommission());
		
		int result = stmt.executeUpdate();
		
		if (result != 1) {
			cust = null;
		}
		
		TravelExpertsDB.closeConnection(conn);
		
		return cust;
	}
	
	public static Customer updateCustomer(Customer cust) throws ClassNotFoundException, SQLException {
		
		Connection conn = TravelExpertsDB.getConnection();
		PreparedStatement stmt = conn.prepareStatement("UPDATE Customers SET"
				+ "	custName=?,"
				+ " custStartDate=?,"
				+ " custEndDate=?,"
				+ " custDesc=?,"
				+ " custBasePrice=?,"
				+ " custAgencyCommission=? "
				+ " WHERE CustomerId=?");
		stmt.setString(1, cust.getcustName());
		stmt.setDate(2, cust.getcustStartDate());
		stmt.setDate(3, cust.getcustEndDate());
		stmt.setString(4, cust.getcustDesc());
		stmt.setDouble(5, cust.getcustBasePrice());
		stmt.setDouble(6, cust.getcustAgencyCommission());
		stmt.setInt(7, cust.getCustomerId());
		
		int result = stmt.executeUpdate();
		
		if (result != 1) {
			cust = null;
		}
		
		TravelExpertsDB.closeConnection(conn);
		
		return cust;
	}
	
	public static int deleteCustomer(int CustomerId) throws ClassNotFoundException, SQLException {
		
		
		
		Connection conn = TravelExpertsDB.getConnection();
		PreparedStatement stmt = conn.prepareStatement("DELETE FROM Customers WHERE CustomerId=?");
		stmt.setInt(1, CustomerId);
		int result = stmt.executeUpdate();
		
		if (result != 1) {
			return result;
		}
		
		TravelExpertsDB.closeConnection(conn);
		
		return result;
	}
	
	
	//TODO: LATER - create rest of the CRUD methods?? depending on scope of project
*/}
