package dataBaseConn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dataObj.Package;

public class PackageDB {
	//TODO: create local variables
	
	//TODO: Create getPackage method
	
	public static List<Package> getAllPackages () throws ClassNotFoundException, SQLException {
		
		Connection conn = TravelExpertsDB.getConnection();
		
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM packages");
		List<Package> pkgs = new ArrayList<Package>();
		Package pkg;
		
		while (rs.next()) {
			pkg = new dataObj.Package();
			pkg.setPackageId(rs.getInt(1));
			pkg.setPkgName(rs.getString(2));
			pkg.setPkgStartDate(rs.getDate(3));
			pkg.setPkgEndDate(rs.getDate(4));
			pkg.setPkgDesc(rs.getString(5));
			pkg.setPkgBasePrice(rs.getDouble(6));
			pkg.setPkgAgencyCommission(rs.getDouble(7));
			
			pkgs.add(pkg);
		}
		
		TravelExpertsDB.closeConnection(conn);
		return pkgs;
		
	}
	
	public static Package getPackage(int packageId) throws ClassNotFoundException, SQLException {
		
		Package pkg = new Package();
		
		Connection conn = TravelExpertsDB.getConnection();
		PreparedStatement stmt = conn.prepareStatement("SELECT * FROM packages WHERE PackageId=?");
		stmt.setInt(1, packageId);
		ResultSet rs = stmt.executeQuery();
		
		if (rs.next()) {
			pkg.setPackageId(rs.getInt(1));
			pkg.setPkgName(rs.getString(2));
			pkg.setPkgStartDate(rs.getDate(3));
			pkg.setPkgEndDate(rs.getDate(4));
			pkg.setPkgDesc(rs.getString(5));
			pkg.setPkgBasePrice(rs.getDouble(6));
			pkg.setPkgAgencyCommission(rs.getDouble(7));
		}
		
		TravelExpertsDB.closeConnection(conn);
		
		return pkg;
	}
	
	public static Package insertPackage(Package pkg) throws ClassNotFoundException, SQLException {

		Connection conn = TravelExpertsDB.getConnection();
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO packages"
				+ " (PkgName, PkgStartDate, PkgEndDate, PkgDesc, PkgBasePrice, PkgAgencyCommission)" 
				+ " VALUES (?,?,?,?,?,?)");
		stmt.setString(1, pkg.getPkgName());
		stmt.setDate(2, pkg.getPkgStartDate());
		stmt.setDate(3, pkg.getPkgEndDate());
		stmt.setString(4, pkg.getPkgDesc());
		stmt.setDouble(5, pkg.getPkgBasePrice());
		stmt.setDouble(6, pkg.getPkgAgencyCommission());
		
		int result = stmt.executeUpdate();
		
		if (result != 1) {
			pkg = null;
		}
		
		TravelExpertsDB.closeConnection(conn);
		
		return pkg;
	}
	
	public static Package updatePackage(Package pkg) throws ClassNotFoundException, SQLException {
		
		Connection conn = TravelExpertsDB.getConnection();
		PreparedStatement stmt = conn.prepareStatement("UPDATE packages SET"
				+ "	PkgName=?,"
				+ " PkgStartDate=?,"
				+ " PkgEndDate=?,"
				+ " PkgDesc=?,"
				+ " PkgBasePrice=?,"
				+ " PkgAgencyCommission=? "
				+ " WHERE PackageId=?");
		stmt.setString(1, pkg.getPkgName());
		stmt.setDate(2, pkg.getPkgStartDate());
		stmt.setDate(3, pkg.getPkgEndDate());
		stmt.setString(4, pkg.getPkgDesc());
		stmt.setDouble(5, pkg.getPkgBasePrice());
		stmt.setDouble(6, pkg.getPkgAgencyCommission());
		stmt.setInt(7, pkg.getPackageId());
		
		int result = stmt.executeUpdate();
		
		if (result != 1) {
			pkg = null;
		}
		
		TravelExpertsDB.closeConnection(conn);
		
		return pkg;
	}
	
	public static int deletePackage(int packageId) throws ClassNotFoundException, SQLException {
		
		
		
		Connection conn = TravelExpertsDB.getConnection();
		PreparedStatement stmt = conn.prepareStatement("DELETE FROM packages WHERE PackageId=?");
		stmt.setInt(1, packageId);
		int result = stmt.executeUpdate();
		
		if (result != 1) {
			return result;
		}
		
		TravelExpertsDB.closeConnection(conn);
		
		return result;
	}
	
	
	//TODO: LATER - create rest of the CRUD methods?? depending on scope of project
}
