package ca.tex.sait.travelexpertsapi;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dataBaseConn.ProductDB;
import dataObj.Product;
/**
 * Written by Derek Poitras
 * 
 * Date: October 6, 2017
 *Purpose: This class handles the Product requests
 */
@Path("/products")
public class products {

	@GET
    @Produces(MediaType.APPLICATION_JSON)
	public List<Product> getAllProduct() {
	
		List<Product> products = new ArrayList<Product>();
		try {
			products = ProductDB.getAllProducts();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return products;
	}

	@GET
	@Path("/{productId}")
    @Produces(MediaType.APPLICATION_JSON)
	public Product getProduct(@PathParam("productId") int productId) {
		
		Product prod = null;
		try {
			prod = ProductDB.getProduct(productId);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return prod;
	}
}