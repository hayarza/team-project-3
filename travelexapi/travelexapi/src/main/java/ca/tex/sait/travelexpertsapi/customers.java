package ca.tex.sait.travelexpertsapi;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dataBaseConn.CustomerDB;
import dataObj.Customer;
/**
 * Written by Derek Poitras
 * 
 * Date: October 6, 2017
 *Purpose: This class handles all the Customer request
 */
@Path("/customers")
public class customers {

	@GET
    @Produces(MediaType.APPLICATION_JSON)
	public List<Customer> getAllCustomer() {
	
		List<Customer> customers = new ArrayList<Customer>();
		try {
			customers = CustomerDB.getAllCustomers();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return customers;
	}

	@GET
	@Path("/{customerId}")
    @Produces(MediaType.APPLICATION_JSON)
	public Customer getCustomer(@PathParam("customerId") int customerId) {
		
		Customer cust = new Customer();
		try {
			cust = CustomerDB.getCustomer(customerId);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return cust;
	}
}