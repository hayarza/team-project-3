package ca.tex.sait.travelexpertsapi;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dataBaseConn.UserDB;
import dataObj.User;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
/**
 * Written by Derek Poitras
 * 
 * Date: October 6, 2017
 *Purpose: This class is to handle the authentication from the client application.
 */
@Path("/author")
public class authenticate {

	//Getting username/pass for authentication from a post of the user class
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User getAuthentication(User pubUser) {
		
		User dbUser = new User();
		
		try {
			dbUser=UserDB.authorizeUser(pubUser.getUserName());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (dbUser.getUserPass().equals(pubUser.getUserPass())) {
			return dbUser;
		}
		
		return null;
	}
	
	// get full user information.
	@GET
	@Path("/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public User getUser(@PathParam("userId") int userId) {
		User user = new User();
		
		try {
			user = UserDB.getUser(userId);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return user;
	}
	
	// JSON web token creation
	public String makeToken () {
		
		String token = "";
		
		try {
			token = Jwts.builder()
					  .setSubject("users/TzMUocMF4p")
					  .setExpiration(new Date(1300819380))
					  .claim("name", "Robert Token Man")
					  .claim("scope", "self groups/admins")
					  .signWith(
					    SignatureAlgorithm.HS256,
					    "secret".getBytes("UTF-8")
					  )
					  .compact();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return token;
	}
	
}
