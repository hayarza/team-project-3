package ca.tex.sait.travelexpertsapi;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import dataBaseConn.PackageDB;
import dataObj.Package;
/**
 * Written by Derek Poitras
 * 
 * Date: October 6, 2017
 *Purpose: This class handles the package request uri's
 */
@Path("/packages")
public class packages {

	// returns a full list of packages
	@GET
    @Produces(MediaType.APPLICATION_JSON)
	public List<Package> getAllPackages() {
	
		List<Package> packages = new ArrayList<Package>();
		try {
			packages = PackageDB.getAllPackages();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return packages;
	}

	//returns a single package
	@GET
	@Path("/{packageId}")
    @Produces(MediaType.APPLICATION_JSON)
	public Package getPackage(@PathParam("packageId") int packageId) {
		
		Package pkg = null;
		try {
			pkg = PackageDB.getPackage(packageId);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return pkg;
	}
	
	//Add a new package to the database
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public Package addPackage(Package pkg) {
		
		Package oldpkg = pkg;
		Package newpkg = new Package();
		
		try {
			newpkg = PackageDB.insertPackage(oldpkg);
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (newpkg == oldpkg) {
			return newpkg;
		} else {
			return null;
		}
	}
	
	//Updates the package in the packages table
	@PUT
	@Path("/{packageId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Package updatePackage(@PathParam("packageId") int packageId, Package pkg) {


		pkg.setPackageId(packageId);
		Package newpkg = new Package();
		
		try {
			newpkg = PackageDB.updatePackage(pkg);
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		if (newpkg.equals(pkg)) {
			return newpkg;//json;
		} else {
			return null;
		}

	}
	
	//delete the package with the supplied id.
	@DELETE
	@Path("/{packageId}")
	@Produces(MediaType.APPLICATION_JSON)
	public int deletePackage (@PathParam("packageId") int packageId) {
		
		int result = 0;
		try {
			result = PackageDB.deletePackage(packageId);
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return result;
		
	}
	

}
