package dataObj;
/**
 * Written by Derek Poitras
 * 
 * Date: October 6, 2017
 *Purpose: User data object class 
 */
public class User {
	
	private int userId;
	private String userName;
	private String userPass;
	private int customerId;
	private int agentId;

	public User () {}

	public User(int userId, String userName, String userPass, int customerId, int agentId) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userPass = userPass;
		this.customerId = customerId;
		this.agentId = agentId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPass() {
		return userPass;
	}

	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}
	
	
}
