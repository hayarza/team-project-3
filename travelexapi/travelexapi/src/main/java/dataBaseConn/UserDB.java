package dataBaseConn;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dataObj.User;
/**
 * Written by Derek Poitras
 * 
 * Date: October 6, 2017
 *Purpose: This is the db class for User interaction with the Database. we created a users Table with 
 *a unique userId, userName, userPass, CustomerId, and AgentId; this alows a single resource for authentication.
 * */
public class UserDB {

	public static User authorizeUser (String uName) throws SQLException, ClassNotFoundException {
		
		User user = new User();
		
		Connection conn = TravelExpertsDB.getConnection();
		PreparedStatement stmt = conn.prepareStatement(""
				+ "SELECT * FROM users "
				+ "WHERE userName=?");
		stmt.setString(1, uName);
		ResultSet rs = stmt.executeQuery();
		
		if (rs.next()) {

			user.setUserId(rs.getInt(1));
			user.setUserName(rs.getString(2));
			user.setUserPass(rs.getString(3));
			user.setCustomerId(rs.getInt(4));
			user.setAgentId(rs.getInt(5));

		}
		
		TravelExpertsDB.closeConnection(conn);
		
		return user;
	}
	
	public static User getUser (int userId) throws SQLException, ClassNotFoundException {
		
		User user = new User();
		
		Connection conn = TravelExpertsDB.getConnection();
		PreparedStatement stmt = conn.prepareStatement(""
				+ "SELECT * FROM users "
				+ "WHERE userId=?");
		stmt.setInt(1, userId);
		ResultSet rs = stmt.executeQuery();
		
		if (rs.next()) {
			user.setUserId(rs.getInt(1));
			user.setUserName(rs.getString(2));
			user.setUserPass(rs.getString(3));
			user.setCustomerId(rs.getInt(4));
			user.setAgentId(rs.getInt(5));

		}
		
		TravelExpertsDB.closeConnection(conn);
		
		return user;
	}
}
