package dataBaseConn;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dataObj.Product;
/**
 * Written by Derek Poitras
 * 
 * Date: October 6, 2017
 *Purpose: This is the db class for Product interaction with the Database
 */
public class ProductDB {
	
	public static List<Product> getAllProducts () throws ClassNotFoundException, SQLException {
		
		Connection conn = TravelExpertsDB.getConnection();
		
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM products");
		List<Product> prods = new ArrayList<Product>();
		Product prod;
		
		while (rs.next()) {
			prod = new dataObj.Product();
			prod.setProductId(rs.getInt(1));
			prod.setProdName(rs.getString(2));

			prods.add(prod);
		}
		
		TravelExpertsDB.closeConnection(conn);
		return prods;
		
	}
	
	public static Product getProduct(int productId) throws ClassNotFoundException, SQLException {
		
		Product prod = new Product();
		
		Connection conn = TravelExpertsDB.getConnection();
		PreparedStatement stmt = conn.prepareStatement("SELECT * FROM products WHERE ProductId=?");
		stmt.setInt(1, productId);
		ResultSet rs = stmt.executeQuery();
		
		if (rs.next()) {
			prod.setProductId(rs.getInt(1));
			prod.setProdName(rs.getString(2));

		}
		
		TravelExpertsDB.closeConnection(conn);
		
		return prod;
	}
}
