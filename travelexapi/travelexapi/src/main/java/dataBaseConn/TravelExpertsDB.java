package dataBaseConn;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * Written by Derek Poitras
 * 
 * Date: October 6, 2017
 *Purpose: This is the db class to open/close connection to the database
 */
public class TravelExpertsDB {
	static Connection conn;
	
	public static Connection getConnection() throws ClassNotFoundException, SQLException {

			Class.forName("org.mariadb.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mariadb://localhost:3306/travelexperts", "logic", "oosd");

		return conn;
	}
	
	public static void closeConnection(Connection c) {
		try {
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
